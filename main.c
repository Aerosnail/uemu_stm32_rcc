#include <assert.h>
#include <errno.h>

#include <module.h>
#include <log/log.h>
#include <utils.h>

#include "commands.h"
#include "rcc.h"

static const char *_compatible[] = {
	"st,stm32g0-rcc",
	"st,stm32-rcc",
	NULL
};

__global
ModuleInfo __info = {
	.name = "stm32-rcc",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible
};

__global int
__init(void)
{
	return 0;
}

__global int
__create_instance(void **dst, ModuleSpecs *specs)
{
	struct rcc *self;
	uint32_t addr, len;
	int ret;

	addr = specs->reg->address;
	len = specs->reg->size;

	ret = rcc_init(&self, specs->name, specs->bus, addr, len);
	if (ret) return ret;

	*dst = self;
	return 0;
}

__global int
__delete_instance(void *ctx)
{
	int ret = 0;
	struct rcc *self = (struct rcc*)ctx;

	ret |= rcc_deinit(self);
	//ret |= unregister_commands(name);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}
