#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <memory.h>
#include <gpio.h>

struct rcc_regs {
	uint32_t cr;
	uint32_t icscr;
	uint32_t cfgr;
	uint32_t pllcfgr;

	uint32_t cier;
	uint32_t cifr;
	uint32_t cicr;

	uint32_t iopenr;
	uint32_t ahbenr;
	uint32_t apbenr1;
	uint32_t apbenr2;
	uint32_t iopsmenr;
	uint32_t ahbsmenr;
	uint32_t apbsmenr1;
	uint32_t apbsmenr2;

	uint32_t ccipr;
	uint32_t ccipr2;
	uint32_t bdcr;

	uint32_t csr;
};

struct rcc {
	MemBus *bus;
	MemEntry *mem;

	struct rcc_regs regs;

	GPIO *io_port_reset[32];    /* IOPRSTR */
	GPIO *ahb_reset[32];        /* AHBRSTR */
	GPIO *apb1_reset[32];       /* APBRSTR1 */
	GPIO *apb2_reset[32];       /* APBRSTR2 */

};

int rcc_init(struct rcc **dst, const char *name, MemBus *bus, uint64_t addr, size_t len);
int rcc_deinit(struct rcc *self);
