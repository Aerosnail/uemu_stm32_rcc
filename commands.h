#pragma once

#include "rcc.h"

int register_commands(const char *name, struct rcc *ctx);
int unregister_commands(const char *name);
