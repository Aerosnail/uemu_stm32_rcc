#pragma once

#include <utils.h>

REG_DEF(cr_pllrdy, 0x1, 25)
REG_DEF(cr_pllon, 0x1, 24)
REG_DEF(cr_hsi48rdy, 0x1, 23)
REG_DEF(cr_hsi48on, 0x1, 22)
REG_DEF(cr_hserdy, 0x1, 17)
REG_DEF(cr_hseon, 0x1, 16)
REG_DEF(cr_hsirdy, 0x1, 10)
REG_DEF(cr_hsion, 0x1, 8)

REG_DEF(bdcr_lsecssd, 0x1, 5)
REG_DEF(bdcr_lserdy, 0x1, 1)
REG_DEF(bdcr_lseon, 0x1, 0)

REG_DEF(csr_lsirdy, 0x1, 1)
REG_DEF(csr_lsion, 0x1, 0)
