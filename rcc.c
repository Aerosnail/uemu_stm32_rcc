#include <errno.h>
#include <inttypes.h>

#include <gpio.h>
#include <memory.h>
#include <log/log.h>
#include <utils.h>

#include "rcc.h"
#include "registers.h"

#define RCC_ADDR_LEN 0x400

enum regs {
	RCC_CR          = 0x00,
	RCC_ICSCR       = 0x04,
	RCC_CFGR        = 0x08,
	RCC_PLLCFGR     = 0x0c,

	RCC_CRRCR       = 0x14,
	RCC_CIER        = 0x18,
	RCC_CIFR        = 0x1c,
	RCC_CICR        = 0x20,
	RCC_IOPRSTR     = 0x24,
	RCC_AHBRSTR     = 0x28,
	RCC_APBRSTR1    = 0x2c,
	RCC_APBRSTR2    = 0x30,
	RCC_IOPENR      = 0x34,
	RCC_AHBENR      = 0x38,
	RCC_APBENR1     = 0x3c,
	RCC_APBENR2     = 0x40,
	RCC_IOPSMENR    = 0x44,
	RCC_AHBSMENR    = 0x48,
	RCC_APBSMENR1   = 0x4c,
	RCC_APBSMENR2   = 0x50,
	RCC_CCIPR       = 0x54,
	RCC_CCIPR2      = 0x58,
	RCC_BDCR        = 0x5c,
	RCC_CSR         = 0x60,
};

static int rcc_read(void *ctx, uint64_t addr, void *data, size_t len);
static int rcc_write(void *ctx, uint64_t addr, const void *data, size_t len);

int
rcc_init(struct rcc **dst, const char *name, MemBus *bus, uint64_t addr, size_t len)
{
	char pin_name[6];
	struct rcc *self;
	struct mem_handler handler;
	size_t pin_num;

	self = calloc(1, sizeof(*self));
	if (!self) return ENOENT;

	self->bus = bus;

	handler.read = rcc_read;
	handler.write = rcc_write;
	handler.execute = rcc_read;
	handler.ctx = self;

	for (pin_num = 0; pin_num < LEN(self->io_port_reset); pin_num++) {
		sprintf(pin_name, ":0:%d", (int)pin_num);
		self->io_port_reset[pin_num] =
			gpio_create_instance(name, pin_name, NULL, NULL);
		gpio_write(self->io_port_reset[pin_num], 0);
		gpio_set_type(self->io_port_reset[pin_num], GPIO_TYPE_OUTPUT_PP);

		sprintf(pin_name, ":1:%d", (int)pin_num);
		self->ahb_reset[pin_num] =
			gpio_create_instance(name, pin_name, NULL, NULL);
		gpio_write(self->ahb_reset[pin_num], 0);
		gpio_set_type(self->ahb_reset[pin_num], GPIO_TYPE_OUTPUT_PP);

		sprintf(pin_name, ":2:%d", (int)pin_num);
		self->apb1_reset[pin_num] =
			gpio_create_instance(name, pin_name, NULL, NULL);
		gpio_write(self->apb1_reset[pin_num], 0);
		gpio_set_type(self->apb1_reset[pin_num], GPIO_TYPE_OUTPUT_PP);

		sprintf(pin_name, ":3:%d", (int)pin_num);
		self->apb2_reset[pin_num] =
			gpio_create_instance(name, pin_name, NULL, NULL);
		gpio_write(self->apb2_reset[pin_num], 0);
		gpio_set_type(self->apb2_reset[pin_num], GPIO_TYPE_OUTPUT_PP);
	}

	self->mem = mem_register(bus, &handler, addr, len);

	self->regs.cr = 0x00000500;

	*dst = self;
	return 0;
}

int
rcc_deinit(struct rcc *self)
{
	int ret = 0;
	size_t pin_num;

	for (pin_num = 0; pin_num < LEN(self->io_port_reset); pin_num++) {
		ret |= gpio_delete_instance(self->io_port_reset[pin_num]);
		ret |= gpio_delete_instance(self->ahb_reset[pin_num]);
		ret |= gpio_delete_instance(self->apb1_reset[pin_num]);
		ret |= gpio_delete_instance(self->apb2_reset[pin_num]);
	}

	ret |= mem_unregister(self->bus, self->mem);

	free(self);

	return ret;
}

/* Memory handlers {{{ */
static int
rcc_read(void *ctx, uint64_t addr, void *data, size_t len)
{
	struct rcc *self = (struct rcc*)ctx;
	uint32_t *data_words = (uint32_t*)data;
	size_t i;

	if (len != 4) return 1;

	switch (addr) {
	case RCC_CR:
		cr_pllrdy_set(&self->regs.cr, cr_pllon_get(self->regs.cr));
		cr_hsi48rdy_set(&self->regs.cr, cr_hsi48on_get(self->regs.cr));
		cr_hsirdy_set(&self->regs.cr, cr_hsion_get(self->regs.cr));
		cr_hserdy_set(&self->regs.cr, cr_hseon_get(self->regs.cr));

		*data_words = self->regs.cr;
		break;

	case RCC_ICSCR:
		*data_words = self->regs.icscr;
		break;

	case RCC_CFGR:
		*data_words = self->regs.cfgr;
		break;

	case RCC_PLLCFGR:
		*data_words = self->regs.pllcfgr;
		break;

	case RCC_CRRCR:
		*data_words = 0;
		break;

	case RCC_CIER:
	case RCC_CIFR:
	case RCC_CICR:
		log_warn("CLK interrupts not implemented");
		break;

	case RCC_IOPRSTR:
		for (i=0; i<32; i++) {
			*data_words |= gpio_read(self->io_port_reset[i]) << (31 - i);
		}
		break;

	case RCC_AHBRSTR:
		for (i=0; i<32; i++) {
			*data_words |= gpio_read(self->ahb_reset[i]) << (31 - i);
		}
		break;

	case RCC_APBRSTR1:
		for (i=0; i<32; i++) {
			*data_words |= gpio_read(self->apb1_reset[i]) << (31 - i);
		}
		break;

	case RCC_APBRSTR2:
		for (i=0; i<32; i++) {
			*data_words |= gpio_read(self->apb2_reset[i]) << (31 - i);
		}
		break;

	case RCC_IOPENR:
		*data_words = self->regs.iopenr;
		break;

	case RCC_AHBENR:
		*data_words = self->regs.ahbenr;
		break;

	case RCC_APBENR1:
		*data_words = self->regs.apbenr1;
		break;

	case RCC_APBENR2:
		*data_words = self->regs.apbenr2;
		break;

	case RCC_IOPSMENR:
		*data_words = self->regs.iopsmenr;
		break;

	case RCC_AHBSMENR:
		*data_words = self->regs.ahbsmenr;
		break;

	case RCC_APBSMENR1:
		*data_words = self->regs.apbsmenr1;
		break;

	case RCC_APBSMENR2:
		*data_words = self->regs.apbsmenr2;
		break;

	case RCC_CCIPR:
		*data_words = self->regs.ccipr;
		break;

	case RCC_CCIPR2:
		*data_words = self->regs.ccipr2;
		break;

	case RCC_BDCR:
		bdcr_lsecssd_set(&self->regs.bdcr, 0);
		bdcr_lserdy_set(&self->regs.bdcr, bdcr_lseon_get(self->regs.bdcr));

		*data_words = self->regs.bdcr;
		break;

	case RCC_CSR:
		csr_lsirdy_set(&self->regs.csr, csr_lsion_get(self->regs.csr));

		*data_words = self->regs.csr;
		break;

	default:
		log_debug("Unimplemented RCC read: 0x%08"PRIx64, addr);
		return 1;
	}


	return 0;
}

static int
rcc_write(void *ctx, uint64_t addr, const void *data, size_t len)
{
	struct rcc *self = (struct rcc*)ctx;
	uint32_t *data_words = (uint32_t*)data;
	int i;

	if (len != 4) return 1;

	switch (addr) {
	case RCC_CR:
		self->regs.cr = *data_words;
		break;

	case RCC_ICSCR:
		self->regs.icscr = *data_words;
		break;

	case RCC_CFGR:
		self->regs.cfgr = *data_words;
		break;

	case RCC_CRRCR:
		break;

	case RCC_CIER:
	case RCC_CIFR:
	case RCC_CICR:
		log_warn("CLK interrupts not implemented");
		break;

	case RCC_IOPRSTR:
		for (i=0; i<32; i++) {
			if (*data_words & (1 << i)) {
				/* Edge trigger, idk */
				gpio_write(self->io_port_reset[i], 1);
				gpio_write(self->io_port_reset[i], 0);
			}
		}
		break;

	case RCC_AHBRSTR:
		for (i=0; i<32; i++) {
			if (*data_words & (1 << i)) {
				gpio_write(self->ahb_reset[i], 1);
				gpio_write(self->ahb_reset[i], 0);
			}
		}
		break;

	case RCC_APBRSTR1:
		for (i=0; i<32; i++) {
			if (*data_words & (1 << i)) {
				gpio_write(self->apb1_reset[i], 1);
				gpio_write(self->apb1_reset[i], 0);
			}
		}
		break;

	case RCC_APBRSTR2:
		for (i=0; i<32; i++) {
			if (*data_words & (1 << i)) {
				gpio_write(self->apb2_reset[i], 1);
				gpio_write(self->apb2_reset[i], 0);
			}
		}
		break;

	case RCC_IOPENR:
		self->regs.iopenr = *data_words;
		break;

	case RCC_AHBENR:
		self->regs.ahbenr = *data_words;
		break;

	case RCC_APBENR1:
		self->regs.apbenr1 = *data_words;
		break;

	case RCC_APBENR2:
		self->regs.apbenr2 = *data_words;
		break;

	case RCC_IOPSMENR:
		self->regs.iopsmenr = *data_words;
		break;

	case RCC_AHBSMENR:
		self->regs.ahbsmenr = *data_words;
		break;

	case RCC_APBSMENR1:
		self->regs.apbsmenr1 = *data_words;
		break;

	case RCC_APBSMENR2:
		self->regs.apbsmenr2 = *data_words;
		break;

	case RCC_CCIPR:
		self->regs.ccipr = *data_words;
		break;

	case RCC_CCIPR2:
		self->regs.ccipr2 = *data_words;
		break;

	case RCC_BDCR:
		self->regs.bdcr = *data_words;
		break;

	default:
		log_debug("Unimplemented RCC write: 0x%08"PRIx64, addr);
		return 1;

	}

	return 0;
}
/* }}} */
